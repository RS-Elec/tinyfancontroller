TinyFanController (TFC)

Small fan control unit to control two 4-Pin PWM fans.

Target Features:

    - Two independent fan channels according Intel PWM Fan Spec
    - Two temperature sensor connectors for either (see TempSensor.h for config details)
        - NTC sensors (10k recommended and tested)
        - KTY81-2XX PTC sensors
        - digital DS18X20 sensors
    - Internal temperature sensor
    - Three user programmed speed curves per fan (one for each sensor)
    - SCPI command set via USB
    - TODO: USB configurable via Python GUI
    - Small form factor
    - Ultra low cost
    - optional human interface
        - 128x32 pixel SSD1306 OLED
        - Two user buttons



The TFC could be configured by SCPI commands either over the USB connection or over the display and pushbuttons if assembled.

The USB ports acts as virtual com port use 8N1 with Baud rate 115200. Command termination is CR (\r, 0x0D) by default.

Since commands must be sent continuously HTerm is recommended as terminal program as long as there is no GUI available.
You need to set DTR in any terminal program to make the Arduino pro micro communicate via USB!

Replace # in the commands by channel/temperature index. Commands with ? in the end request the actual value, commands without will set the value.
You can short all commands to the upper case string part, commands are not case sensitive at all.

All temperature values have a accuracy of 0.5 °C, the internal sensor of 1 °C due to Atmel hardware limits.

SCPI commands (and menu items):

    *IDN?
        Request identification string from device. Typical response is:
        "rselec,TinyFanController,1.00<\r>"
        Version number in the end will change.

    RPM#?
        Returns the actual fan speed of a channel in rotations per minute.
    
    POWer#?
        Returns the actual power (PWM duty cycle) in percent.
        
    Temp#?
        Returns the actual temperature of a sensor in °C. Channel 2 is the internal sensor, it measure the internal MCU temperature which is
        usually higer than the air around.
            
    STAtus#?
        Returns the status of a fan channel, it can be either: ON, OFF or START.
        ON means that the fan is spinning as controled, OFF means that the fan is stopped intentionally.
        During START the fan gets 100% to speed up for the time defined by STARTuptime.
        
    ENOvertemp
        Enables or disables the over temperature alert. Use "0" or "DISABLE" and "1" or "ENABLE" as parameters.
        The TFC does nothing in particular at an overtemp condition besides set power of the fan to 100 %.    
    
    ENOvertemp?
        Returns if the over temperature alert is enabled or not.
    
    ENFanblock
        Enables or disables the fan block alert. Use "0" or "DISABLE" and "1" or "ENABLE" as parameters.
        The TFC does nothing in particular at an fan block condition.    
    
    ENFanblock?
        Returns if the fan block alert is enabled or not.
    
    OVERtemp? 
        Checks for an over temperature alert. It is asserted when a temperature acceeds its given maximum temperature in one ore more fan channels.
    
    FANBlock?
        Checks for a fan block alert. It is asserted when it is enabled and a fan should spin in the ON state but no tacho signal is present.
        
    CHannel#:MINTemp#
        Sets the minimum temperature of a temperature sensor for a fan channel. This is the starting point for the fan to spin (if it turns off) or
        to spin faster than the minimum power. The speed is scaled linear to the related maximum temperature given. A fan channel could be related to
        more than one sensor, the highest calculated speed would be used. Set to 125.0 °C if not used.        
        
    CHannel#:MINTemp#?
        Returns the configured minimum temperature.
    
    CHannel#:MAXTemp#
        Sets the maximum temperature of a temperature sensor for a fan channel. This is the 100% speed point for the fan. The speed is scaled linear
        from minimum to the maximum temperature given. A fan channel could be related to more than one sensor, the highest calculated speed would be used.
        Set to 127.5 °C if not used.    
        
    CHannel#:MAXTemp#?
        Returns the configured maximum temperature.
    
    CHannel#:MANualpower
        Sets the manual fan power (PWM duty cycle) in percent. This speed is used if the fan channel is in manual mode.
        
    CHannel#:MANualpower?  
        Returns the manual fan power (PWM duty cycle) in percent.
        
    CHannel#:MINPower
        Set the minimum fan power in percent it needs to spin at all. Fans behave different below 20% and some go to 100% if the duty cycle is to low.
    
    CHannel#:MINPower?
        Returns the minimum fan power (PWM duty cycle) in percent.
    
    CHannel#:STARTuptime
        Set the startup time in seconds. If the fan was turned off before, it will speed up to 100% for the given time until it returns to the
        automatic or manual speed. Most modern fans will take care of correct startup by themself, set to 0 if not needed at all.
    
    CHannel#:STARTuptime?
        Returns the startup time in seconds.
    
    CHannel#:AUTOmaticmode
        Enables or disables the automatic speed control of a fan channel. Use "0" or "DISABLE" and "1" or "ENABLE" as parameters. If enabled the
        fan speed is determined by the configured min and max temperatures, the minimum power and whether stop is enabled or not. If disabled the fan
        speed is set to the configured manual power level, regardless of temperture values.
        
    CHannel#:AUTOmaticmode?
        Returns whether automatic mode is enabled.
    
    CHannel#:STOPenable
        Defines if the fan is allowed to stop if no cooling is required in automatic mode. Use "0" or "DISABLE" and "1" or "ENABLE" as parameters.
        This should only enabled when the fan really stops at 0% duty cycle. Some fans do, some still spin at minimum speed and some go to full speed.
        Test your fan in manual mode before setting this up.
    
    CHannel#:STOPenable?
        Returns whether the fan is allowed to stop.
    
    CHannel#:THReshold
        This thershold value in percent defines when the fan is released from stop mode. It prevents a channel starting and stopping a fan all the time when
        cooling demand is low. Increase value if this is observed. This value is only used in automatic mode with fan stop enabled.
        
    CHannel#:THReshold?
        Returns the hershold value in percent.

