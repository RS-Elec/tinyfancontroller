// -----------------------------------------------------------------------------------------
// --------------------------------------- includes ----------------------------------------
// -----------------------------------------------------------------------------------------

#include <Arduino.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include "oled.h"
#include "Timer.h"
#include "TempSensor.h"
#include "Vrekrer_scpi_parser.h"
// add #include "..\..\..\..\..\include\Vrekrer_scpi_parser_conf.h" into this header file!


// -----------------------------------------------------------------------------------------
// ------------------------------------ defines --------------------------------------------
// -----------------------------------------------------------------------------------------

#define FIRMWAREVERSION   "1.03"
#define DECIMAL_SEPERATOR ','
// #define HMI                         // define if you have display and switch connected

// Mark of the EEPROMVERSION, it is independent of the Firmware Version.
// Only increment it, when changes of the existing data format was made.
#define EEPROMVERSION 2
#define EEPROMVERSION_BITS (0x0100 * EEPROMVERSION + EEPROMVERSION)

// Default values, change it here.
#define DEFAULT_CHANNEL_DATA_VALUES {								\
                                        {250, 250, 250},			\
                                        {255, 255, 255},			\
                                        100, 20, 3, 0, 0, 10}

#define DEFAULT_ALERT_DATA_VALUES {0, 0}

// Button press durations in DURATION_STEPSIZE steps
#define DURATION_STEPSIZE 20  // in ms	
#define SHORT_DURATION     2  // should be at least 2 to have effective debouncing	
#define LONG_DURATION	  50  // must be between SHORT_DURATION and MAX_DURATION
#define MAX_DURATION     127  // 0x7F

// redefine the F(...) Makro to reuse the Progmem String elsewere
//#define F(string_literal) (reinterpret_cast<const __FlashStringHelper *>(PSTR(string_literal)))
#define F_PSTR(string_literal) (reinterpret_cast<const __FlashStringHelper *>(string_literal))



// -----------------------------------------------------------------------------------------
// -------------------------- global type definitions --------------------------------------
// -----------------------------------------------------------------------------------------

enum Stati{off, startup, on};

/**
 * @brief CHANNELDATA contains the permanent data of a channel.
 */
typedef struct{						
    uint8_t minTemp[SENSOR_CHANNELS];	     	/**< minimum temperatures */
    uint8_t maxTemp[SENSOR_CHANNELS]; 			/**< maximum temperatures */
    uint8_t manualPower;						/**< the power in manual mode */
    uint8_t minimumPower;						/**< the minimum power in automatic mode */
    uint8_t startupTime;						/**< startup time [s] */
    uint8_t automaticMode;					    /**< if 1 automatic mode enabled */
    uint8_t stopEnable;							/**< if 1 fan is able to stop in automatic mode */
    uint8_t threshold;							/**< a threshold value in % for the start in automatic mode */
}CHANNELDATA;									// 12 bytes

/**
 * @brief CHANNELSTATUS contains the status (non permanent data) of a channel.
 */
typedef struct{						
    uint16_t rpm;								/**< rotation per minute */
    uint8_t power;								/**< the actual power of the channel in % */
    uint8_t startupTimer;						/**< the timer witch counts the startup time down */
    enum Stati status;							/**< the status of the channel */	 
}CHANNELSTATUS;									// 5 bytes

/**
 * @brief ALERTDATA contains the enable switches for all alerts.
 */
typedef struct{						
    uint8_t overtempEnable;						/**< 1 if over temperture alert enabled */
    uint8_t fanblockEnable;						/**< 1 if fan block alert enabled */
}ALERTDATA;										// 2 bytes

/**
 * @brief ALERTSTATUS contains the status of all alerts.
 */
typedef struct{						
    uint8_t overtemp;							/**< 1 if over temperture alert */
    uint8_t fanblock;							/**< 1 if a fan is blocked */
}ALERTSTATUS;		

typedef enum gpio_presstime
{
    SHORT,				// short button press
    LONG				// long button press 
} gpio_presstime_t;

typedef enum gpio_button
{
    BUTTON_UP   = 0,
    BUTTON_DOWN = 1,	
    BUTTON_MAX				
} gpio_button_t;

typedef enum menu_item
{
    MI_EXIT    =  0,
    MI_VERSION =  1,	
    MI_ENO     =  2,	
    MI_ENF     =  3,	
    MI_MINT    =  4,	
    MI_MAXT    =  5,	
    MI_MAN     =  6,	
    MI_MINP    =  7,	
    MI_START   =  8,	
    MI_AUTO    =  9,	
    MI_STOP    = 10,	
    MI_THR     = 11,	
    MI_MAX				
} menu_item_t;



// -----------------------------------------------------------------------------------------
// -------------------------- global eeprom definitions ------------------------------------
// -----------------------------------------------------------------------------------------

uint16_t EEMEM eeprom_eepromVersionChecker = EEPROMVERSION_BITS;
CHANNELDATA EEMEM eeprom_channel_data[FAN_CHANNELS] = {DEFAULT_CHANNEL_DATA_VALUES, DEFAULT_CHANNEL_DATA_VALUES};
ALERTDATA EEMEM	eeprom_alert_data = DEFAULT_ALERT_DATA_VALUES;



// -----------------------------------------------------------------------------------------
// ------------------------ global function definitions ------------------------------------
// -----------------------------------------------------------------------------------------

char* temp2str(uint8_t temp, char* aString);							// converts a temperature value into a human readable string
int8_t findNumberInString(char* aString);
void display_statusscreen(void); 										// show main screen on oled display
bool perform_menu(void);


// SCPI parsing functions, see end of file for implementations
void Identify(SCPI_C commands, SCPI_P parameters, Stream& interface);
void GetRpm(SCPI_C commands, SCPI_P parameters, Stream& interface);        
void GetPower(SCPI_C commands, SCPI_P parameters, Stream& interface);
void GetTemp(SCPI_C commands, SCPI_P parameters, Stream& interface); 
void GetStatus(SCPI_C commands, SCPI_P parameters, Stream& interface);
void SetOvertempEnable(SCPI_C commands, SCPI_P parameters, Stream& interface);
void GetOvertempEnable(SCPI_C commands, SCPI_P parameters, Stream& interface);
void SetFanblockEnable(SCPI_C commands, SCPI_P parameters, Stream& interface);
void GetFanblockEnable(SCPI_C commands, SCPI_P parameters, Stream& interface);
void GetOvertemp(SCPI_C commands, SCPI_P parameters, Stream& interface);
void GetFanblock(SCPI_C commands, SCPI_P parameters, Stream& interface);
void SetMinTemp(SCPI_C commands, SCPI_P parameters, Stream& interface);
void GetMinTemp(SCPI_C commands, SCPI_P parameters, Stream& interface);
void SetMaxTemp(SCPI_C commands, SCPI_P parameters, Stream& interface);
void GetMaxTemp(SCPI_C commands, SCPI_P parameters, Stream& interface);
void SetManualPower(SCPI_C commands, SCPI_P parameters, Stream& interface);
void GetManualPower(SCPI_C commands, SCPI_P parameters, Stream& interface);
void SetMinimumPower(SCPI_C commands, SCPI_P parameters, Stream& interface);
void GetMinimumPower(SCPI_C commands, SCPI_P parameters, Stream& interface);
void SetStartupTime(SCPI_C commands, SCPI_P parameters, Stream& interface);
void GetStartupTime(SCPI_C commands, SCPI_P parameters, Stream& interface);
void SetAutomaticMode(SCPI_C commands, SCPI_P parameters, Stream& interface);
void GetAutomaticMode(SCPI_C commands, SCPI_P parameters, Stream& interface);
void SetStopEnable(SCPI_C commands, SCPI_P parameters, Stream& interface);
void GetStopEnable(SCPI_C commands, SCPI_P parameters, Stream& interface);
void SetThreshold(SCPI_C commands, SCPI_P parameters, Stream& interface);
void GetThreshold(SCPI_C commands, SCPI_P parameters, Stream& interface);


// GPIO functions for buttons
bool gpio_get(gpio_button_t item);
void gpio_poll(void);
bool gpio_checkButton(gpio_button_t button, gpio_presstime_t duration);



// -----------------------------------------------------------------------------------------
// ------------------------ global variable declarations -----------------------------------
// -----------------------------------------------------------------------------------------

const char fstr_auto[] PROGMEM = "AUTO";
const char fstr_manual[] PROGMEM = "MANL";
const char fstr_enabled[] PROGMEM = "ENABLED";
const char fstr_disabled[] PROGMEM = "DISABLED";


const CHANNELDATA PROGMEM default_channel_data = DEFAULT_CHANNEL_DATA_VALUES;
const ALERTDATA	PROGMEM default_alert_data = DEFAULT_ALERT_DATA_VALUES;

CHANNELDATA channel_data[FAN_CHANNELS];		// the permanent data of the channels (fan 0-1)
CHANNELSTATUS channel_status[FAN_CHANNELS];	// the actual status of the channels
ALERTDATA alert_data;				        // data for alert
ALERTSTATUS alert_status;			        // actual status
uint8_t temperature[SENSOR_CHANNELS];       // the up to date temperatures of the sensors

// Byte for each button is splitted in two parts:
// MSB marks if button is pressed (0) or released (1).
// Bit 0 to 6 counts how long the button is/was pressed.
uint8_t buttonCtr[BUTTON_MAX] = {0};

// Flash string declarations for commands and menue
const char fstr_IDNr[]   PROGMEM = "*IDN?";
const char fstr_RPMr[]   PROGMEM = "RPM#?";            
const char fstr_POWr[]   PROGMEM = "POWer#?";          
const char fstr_Tr[]     PROGMEM = "Temp#?";           
const char fstr_STAr[]   PROGMEM = "STAtus#?";         
const char fstr_ENO[]    PROGMEM = "ENOvertemp";        // M
const char fstr_ENOr[]   PROGMEM = "ENOvertemp?";      
const char fstr_ENF[]    PROGMEM = "ENFanblock";        // M
const char fstr_ENFr[]   PROGMEM = "ENFanblock?";      
const char fstr_OVERr[]  PROGMEM = "OVERtemp?";       
const char fstr_FANBr[]  PROGMEM = "FANBlock?";        
const char fstr_CH[]     PROGMEM = "CHannel#";          
const char fstr_MINT[]   PROGMEM = ":MINTemp#";         // M
const char fstr_MINTr[]  PROGMEM = ":MINTemp#?";     
const char fstr_MAXT[]   PROGMEM = ":MAXTemp#";         // M
const char fstr_MAXTr[]  PROGMEM = ":MAXTemp#?";     
const char fstr_MAN[]    PROGMEM = ":MANualpower";      // M
const char fstr_MANr[]   PROGMEM = ":MANualpower?";  
const char fstr_MINP[]   PROGMEM = ":MINPower";         // M
const char fstr_MINPr[]  PROGMEM = ":MINPower?";     
const char fstr_START[]  PROGMEM = ":STARTuptime";      // M
const char fstr_STARTr[] PROGMEM = ":STARTuptime?";  
const char fstr_AUTO[]   PROGMEM = ":AUTOmaticmode";    // M
const char fstr_AUTOr[]  PROGMEM = ":AUTOmaticmode?";
const char fstr_STOP[]   PROGMEM = ":STOPenable";       // M
const char fstr_STOPr[]  PROGMEM = ":STOPenable?";   
const char fstr_THR[]    PROGMEM = ":THReshold";        // M
const char fstr_THRr[]   PROGMEM = ":THReshold?";


// -----------------------------------------------------------------------------------------
// ------------------------- global object declarations ------------------------------------
// -----------------------------------------------------------------------------------------

SCPI_Parser scpi_tfc;	// SCPI TinyFanController object



// -----------------------------------------------------------------------------------------
// ---------------------------------- main program -----------------------------------------
// -----------------------------------------------------------------------------------------

void setup()
{
  _delay_ms(10);    // did we need this delay?

  scpi_tfc.RegisterCommand(F_PSTR(fstr_IDNr),      &Identify          );
  scpi_tfc.RegisterCommand(F_PSTR(fstr_RPMr),      &GetRpm            );
  scpi_tfc.RegisterCommand(F_PSTR(fstr_POWr),      &GetPower          );
  scpi_tfc.RegisterCommand(F_PSTR(fstr_Tr),        &GetTemp           );
  scpi_tfc.RegisterCommand(F_PSTR(fstr_STAr),      &GetStatus         );
  scpi_tfc.RegisterCommand(F_PSTR(fstr_ENO),       &SetOvertempEnable );
  scpi_tfc.RegisterCommand(F_PSTR(fstr_ENOr),      &GetOvertempEnable );
  scpi_tfc.RegisterCommand(F_PSTR(fstr_ENF),       &SetFanblockEnable );
  scpi_tfc.RegisterCommand(F_PSTR(fstr_ENFr),      &GetFanblockEnable );
  scpi_tfc.RegisterCommand(F_PSTR(fstr_OVERr),     &GetOvertemp       );
  scpi_tfc.RegisterCommand(F_PSTR(fstr_FANBr),     &GetFanblock       );
  scpi_tfc.SetCommandTreeBase(F_PSTR(fstr_CH));
     scpi_tfc.RegisterCommand(F_PSTR(fstr_MINT),     &SetMinTemp        );
     scpi_tfc.RegisterCommand(F_PSTR(fstr_MINTr),    &GetMinTemp        );
     scpi_tfc.RegisterCommand(F_PSTR(fstr_MAXT),     &SetMaxTemp        );
     scpi_tfc.RegisterCommand(F_PSTR(fstr_MAXTr),    &GetMaxTemp        );
     scpi_tfc.RegisterCommand(F_PSTR(fstr_MAN),      &SetManualPower    );
     scpi_tfc.RegisterCommand(F_PSTR(fstr_MANr),     &GetManualPower    );
     scpi_tfc.RegisterCommand(F_PSTR(fstr_MINP),     &SetMinimumPower   );
     scpi_tfc.RegisterCommand(F_PSTR(fstr_MINPr),    &GetMinimumPower   );
     scpi_tfc.RegisterCommand(F_PSTR(fstr_START),    &SetStartupTime    );
     scpi_tfc.RegisterCommand(F_PSTR(fstr_STARTr),   &GetStartupTime    );
     scpi_tfc.RegisterCommand(F_PSTR(fstr_AUTO),     &SetAutomaticMode  );
     scpi_tfc.RegisterCommand(F_PSTR(fstr_AUTOr),    &GetAutomaticMode  );
     scpi_tfc.RegisterCommand(F_PSTR(fstr_STOP),     &SetStopEnable     );
     scpi_tfc.RegisterCommand(F_PSTR(fstr_STOPr),    &GetStopEnable     );
     scpi_tfc.RegisterCommand(F_PSTR(fstr_THR),      &SetThreshold      );
     scpi_tfc.RegisterCommand(F_PSTR(fstr_THRr),     &GetThreshold      );


  Serial.begin(115200);

  #ifdef HMI
    OLED_init(false, 255);
  #endif

  // OLED_print_P(PSTR("TinyFanControl"), 0, 0, font_small);
  // OLED_print_P(PSTR("V:"), 1, 0, font_small);
  // OLED_print_P(PSTR(FIRMWAREVERSION), 1, 17, font_small);

  timer_init();

  // Change Buttons to input
  DDRB &= ~( (1<<4)|(1<<5) ); // KEY1, PB4; KEY0, PB5
  // Enable Pullups on buttons
  PORTB |= ( (1<<4)|(1<<5) );

  // full speed for both fans at startup
  timer_setPwm(0, 100);
  timer_setPwm(1, 100);

  tempSensor_init();
  sei();

  // EEPROM is not OK, restore default values from progmem
  if (eeprom_read_word(&eeprom_eepromVersionChecker) != EEPROMVERSION_BITS)
  {	
    for (uint8_t j=0; j<FAN_CHANNELS; j++)
    {
        for (uint8_t i=0; i<sizeof(CHANNELDATA); i++)
        {
            eeprom_write_byte(((uint8_t *)&eeprom_channel_data[j])+i, pgm_read_byte(((uint8_t *)&default_channel_data)+i));
        }
    }

    for (uint8_t i=0; i<sizeof(ALERTDATA); i++)
    {
        eeprom_write_byte(((uint8_t *)&eeprom_alert_data)+i, pgm_read_byte(((uint8_t *)&default_alert_data)+i));
    }
                
    eeprom_write_word(&eeprom_eepromVersionChecker, EEPROMVERSION_BITS);
  }
            
  // read the EEPROM
  eeprom_read_block((void*)channel_data, (const void*)eeprom_channel_data, (FAN_CHANNELS*sizeof(CHANNELDATA)));
  eeprom_read_block((void*)&alert_data, (const void*)&eeprom_alert_data, sizeof(ALERTDATA));
    
  // reload startup timer and set fan channel status to startup
  for (uint8_t i=0; i<FAN_CHANNELS; i++)
  {
    if (channel_data[i].startupTime == 0)   // no startup needed
    {
        channel_status[i].startupTimer = 0;
        channel_status[i].status = on;
    }
    else
    {
        channel_status[i].status = startup;		// start the channel and reset the startup timer
        channel_status[i].startupTimer = channel_data[i].startupTime;
    }
  }
  // OLED_clear();	
}


// the loop function runs over and over again forever
void loop()
{  
  static uint32_t last_fan_check = 0;
  static uint32_t last_display_refresh = 0;
  static uint32_t last_gpio_poll = 0;
  uint32_t now;
  static bool menue = false;		// becomes true if the menue is displayed
  //char myString[6] = "";

  alert_status.overtemp = 0;				// reset the overtemp alert

  // calculate the channel power
  for (uint8_t i=0; i<FAN_CHANNELS; i++)
  {  			
      if (channel_data[i].automaticMode)		// automatic mode
      {
          uint8_t cooling = 0;			// temporary cooling value
          uint8_t d;					// temporary variable for calculation
          for (uint8_t j=0; j<SENSOR_CHANNELS; j++)
          {
              if ((temperature[j] >= channel_data[i].minTemp[j]) && (temperature[j] <= channel_data[i].maxTemp[j])) // temperature is between min and max
              {
                  // calculate cooling value
                  d = (uint8_t)( ((uint16_t)(temperature[j] - channel_data[i].minTemp[j]) * 100) / (channel_data[i].maxTemp[j] - channel_data[i].minTemp[j]) );

                  if (d > cooling)	// save the highest cooling value	
                  {
                      cooling = d;
                  }						
              }
              if (temperature[j] > channel_data[i].maxTemp[j])	// a temperature over maximum! o.O
              {
                  // over temperature
                  if (alert_data.overtempEnable)
                  {
                      alert_status.overtemp = 1;
                  }					
                  cooling = 100;
              }				
          }
          
          switch (channel_status[i].status)						// check the status
          {
              case off:		
                    channel_status[i].power = 0;					// turn it off
                    if (channel_data[i].stopEnable)					// off allowed
                    {
                      if (cooling > channel_data[i].threshold)	// if cooling is higher then threshold
                      {
                        if (channel_data[i].startupTime == 0)   // no startup needed
                        {
                            channel_status[i].startupTimer = 0;
                            channel_status[i].status = on;
                        }
                        else
                        {
                            channel_status[i].status = startup;		// start the channel and reset the startup timer
                            channel_status[i].startupTimer = channel_data[i].startupTime;
                        }
                      }							
                    }
                    else                                            // off is not allowed (anymore)
                    {
                        if (channel_data[i].startupTime == 0)   // no startup needed
                        {
                            channel_status[i].startupTimer = 0;
                            channel_status[i].status = on;
                        }
                        else
                        {
                            channel_status[i].status = startup;		// start the channel and reset the startup timer
                            channel_status[i].startupTimer = channel_data[i].startupTime;
                        }
                    }
                    break;
                
                case startup:										
                    channel_status[i].power = 100;					// 100 % for startup
                    break;
                
                case on:
                    if ((cooling == 0) && (channel_data[i].stopEnable == 1))	// if stop is enabled and no cooling needed
                    {
                        channel_status[i].status = off;				// turn it off
                    }
                    // calculate the power value out off the minimum value and the cooling value
                    channel_status[i].power = (uint8_t)((((uint16_t)(100-channel_data[i].minimumPower))*cooling)/100 + channel_data[i].minimumPower);
                    break;
            }				
        } // end automatic mode
         
        else								// manual mode
        {
            switch (channel_status[i].status)					// check the status
            {
                case off:						
                    channel_status[i].power = 0;				// turn it off
                    if (channel_data[i].manualPower > 0)		
                    {
                        if (channel_data[i].startupTime == 0)   // no startup needed
                        {
                            channel_status[i].startupTimer = 0;
                            channel_status[i].status = on;
                        }
                        else
                        {
                            channel_status[i].status = startup;		// start the channel and reset the startup timer
                            channel_status[i].startupTimer = channel_data[i].startupTime;
                        }                        
                    }					
                    break;
                
                case startup:
                    channel_status[i].power = 100;				// 100 % for startup
                    break;
                    
                case on:
                    channel_status[i].power = channel_data[i].manualPower;
                    if (channel_data[i].manualPower == 0)		// if no power, go to off
                    {
                        channel_status[i].status = off;
                    }
                    break;						
            }						
        } // end manual mode		
    } // end for every channel

    alert_status.fanblock = 0;		// reset the fan block status

    // exchange channel with hardware timer
    for (uint8_t i=0; i<FAN_CHANNELS; i++)
    {
        timer_setPwm(i,channel_status[i].power);
        channel_status[i].rpm = timer_getRpm(i);
        if (channel_status[i].status == on && channel_status[i].rpm == 0 && alert_data.fanblockEnable && i != 3)
        {
            alert_status.fanblock = 1;		// fan is blocked or not started correctly
        }			
    }


    // each second
    now = millis();
    if ( (now-last_fan_check) >= 1000)
    {			
        last_fan_check = now;
    
        // decrement the channel startup timers
        for (uint8_t i=0; i<FAN_CHANNELS; i++)
        {
            if (channel_status[i].startupTimer > 1)
            {
                channel_status[i].startupTimer--;
            }
            if (channel_status[i].startupTimer == 1)
            {
                channel_status[i].startupTimer = 0;
                channel_status[i].status = on;		// go to on if startup time is over
            }
        }

        // get the temperatures
        for (uint8_t i=0; i<SENSOR_CHANNELS; i++)
        {
            temperature[i] = tempSensor_getTemp(i);		// read out all sensors
        }
        
        //Serial.println(now);
    }


    scpi_tfc.ProcessInput(Serial, SCPI_TERMINATOR);


    now = millis();
    if ( (now-last_gpio_poll) >= DURATION_STEPSIZE)
    {
        last_gpio_poll = now;
        gpio_poll();
    }

    #ifdef HMI
        if (menue)
        {
            menue = perform_menu();       
        }
        else
        {
            // every 1234 ms
            now = millis();
            if ( (now-last_display_refresh) >= 1234)
            {
                last_display_refresh = now;

                display_statusscreen();
                // OLED_printf_P(3, 66, font_small, PSTR("%6us"), last_display_refresh/1000);
            }

            if (gpio_checkButton(BUTTON_UP, LONG) || gpio_checkButton(BUTTON_DOWN, LONG))
            {
                menue = true;
            }
        }
    #endif
} // end main loop


bool perform_menu(void)
{
    static menu_item_t menu_idx = (menu_item_t)0;   // index for menu items, each setting have an item
    static bool update_display = true;              // true if the display needs an update
    static uint8_t menu_level = 0;                  // menu level which is effected by short botton presses,
                                                    // 0 changes the menu index, 1 the value, 2 the channel and 3 the temperture index
    static uint8_t menu_fan_ch = 0;                 // the fan channel which is effected and shown
    static uint8_t menu_t_idx = 0;                  // the min/max temperatur to be changed
    int8_t value_crement = 0;                       // remember if the value of the actual menu item
                                                    //  should be incremented or decremented (once)
    char myString[6] = "";

    if (gpio_checkButton(BUTTON_UP, SHORT))
    {
        if (menu_level == 0)
        {
            if (menu_idx > 0)
            {
            menu_idx = (menu_item_t)(((uint8_t)menu_idx) - 1);
            }
            else
            {
                menu_idx = (menu_item_t)(MI_MAX-1);
            }            
        }
        else if (menu_level == 2)
        {
            if (menu_fan_ch < FAN_CHANNELS-1)
            {
                menu_fan_ch ++;
            }            
        }
        else if (menu_level == 3)
        {
            if (menu_t_idx < SENSOR_CHANNELS-1)
            {
                menu_t_idx ++;
            }            
        }
        else
        {
            value_crement = 1;
        }
        update_display = true;       
    }

    if (gpio_checkButton(BUTTON_DOWN, SHORT))
    {        
        if (menu_level == 0)
        {
            if (menu_idx < MI_MAX-1)
            {
                menu_idx = (menu_item_t)(((uint8_t)menu_idx) + 1);
            }
            else
            {
                menu_idx = (menu_item_t)0;
            }           
        }
        else if (menu_level == 2)
        {
            if (menu_fan_ch > 0)
            {
                menu_fan_ch --;
            }
        }
        else if (menu_level == 3)
        {
            if (menu_t_idx > 0)
            {
                menu_t_idx --;
            }            
        }
        else
        {
            value_crement = -1;
        }
        update_display = true;
    }

    if (gpio_checkButton(BUTTON_DOWN, LONG))
    {
        uint8_t menu_maxlevel = 0;
        switch (menu_idx)
        {
        case MI_VERSION:
            menu_maxlevel = 0;     // the version menu has no value and nothing to change
            break;
        
        case MI_EXIT:           // this menues have only a value to change
        case MI_ENO:
        case MI_ENF:
            menu_maxlevel = 1;
            break;

        case MI_MINT:
        case MI_MAXT:
            menu_maxlevel = 3;
            break;
            
        case MI_MAN:
        case MI_MINP:
        case MI_START:
        case MI_AUTO:
        case MI_STOP:
        case MI_THR:
            menu_maxlevel = 2;
            break;
        }        
        if (menu_level < menu_maxlevel)
        {
            menu_level++;
            update_display = true;
        }
    }

    if (gpio_checkButton(BUTTON_UP, LONG))
    {
        if (menu_level > 0)
        {
            menu_level--;
            update_display = true;
        }
    }
    
    switch (menu_idx)
    {
    case MI_EXIT:
        if (menu_level == 1)
        {
            OLED_clear();          

            // update all eeprom values
            eeprom_update_byte(&eeprom_alert_data.overtempEnable, alert_data.overtempEnable);
            eeprom_update_byte(&eeprom_alert_data.fanblockEnable, alert_data.fanblockEnable);
            for (menu_fan_ch = 0; menu_fan_ch < FAN_CHANNELS; menu_fan_ch++)
            {
                for (menu_t_idx = 0; menu_t_idx < SENSOR_CHANNELS; menu_t_idx++)
                {
                    eeprom_update_byte(&eeprom_channel_data[menu_fan_ch].minTemp[menu_t_idx], channel_data[menu_fan_ch].minTemp[menu_t_idx]);
                    eeprom_update_byte(&eeprom_channel_data[menu_fan_ch].maxTemp[menu_t_idx], channel_data[menu_fan_ch].maxTemp[menu_t_idx]);
                }               
                eeprom_update_byte(&eeprom_channel_data[menu_fan_ch].manualPower, channel_data[menu_fan_ch].manualPower);
                eeprom_update_byte(&eeprom_channel_data[menu_fan_ch].minimumPower, channel_data[menu_fan_ch].minimumPower);
                eeprom_update_byte(&eeprom_channel_data[menu_fan_ch].startupTime, channel_data[menu_fan_ch].startupTime);
                eeprom_update_byte(&eeprom_channel_data[menu_fan_ch].automaticMode, channel_data[menu_fan_ch].automaticMode);
                eeprom_update_byte(&eeprom_channel_data[menu_fan_ch].stopEnable, channel_data[menu_fan_ch].stopEnable);
                eeprom_update_byte(&eeprom_channel_data[menu_fan_ch].threshold, channel_data[menu_fan_ch].threshold);
            }
            
            update_display = true;
            menu_level = 0;
            menu_fan_ch = 0;
            menu_t_idx = 0;
            
            return false;
        }
        break;
    
    case MI_ENO:
        if (value_crement == 1)
        {
            alert_data.overtempEnable = 1;
        }
        else if (value_crement == -1)
        {
            alert_data.overtempEnable = 0;
        }        
        break;

    case MI_ENF:
        if (value_crement == 1)
        {
            alert_data.fanblockEnable = 1;
        }
        else if (value_crement == -1)
        {
            alert_data.fanblockEnable = 0;
        }
        break;

    case MI_MINT:
        channel_data[menu_fan_ch].minTemp[menu_t_idx] += value_crement;
        if (channel_data[menu_fan_ch].minTemp[menu_t_idx] > 250)
        {
            channel_data[menu_fan_ch].minTemp[menu_t_idx] = 250;
        }        
        break;

    case MI_MAXT:
        channel_data[menu_fan_ch].maxTemp[menu_t_idx] += value_crement;
        break;
        
    case MI_MAN:
        channel_data[menu_fan_ch].manualPower += value_crement;
        if (channel_data[menu_fan_ch].manualPower > 100)
        {
            channel_data[menu_fan_ch].manualPower = 100;
        }
        break;
        
    case MI_MINP:
        channel_data[menu_fan_ch].minimumPower += value_crement;
        if (channel_data[menu_fan_ch].minimumPower > 100)
        {
            channel_data[menu_fan_ch].minimumPower = 100;
        }
        break;
        
    case MI_START:
        channel_data[menu_fan_ch].startupTime += value_crement;
        break;
        
    case MI_AUTO:
        if (value_crement == 1)
        {
            channel_data[menu_fan_ch].automaticMode = 1;
        }
        else if (value_crement == -1)
        {
            channel_data[menu_fan_ch].automaticMode = 0;
        }
        break;
        
    case MI_STOP:
        if (value_crement == 1)
        {
            channel_data[menu_fan_ch].stopEnable = 1;
        }
        else if (value_crement == -1)
        {
            channel_data[menu_fan_ch].stopEnable = 0;
        }
        break;
        
    case MI_THR:
        channel_data[menu_fan_ch].threshold += value_crement;
        if (channel_data[menu_fan_ch].threshold > 100)
        {
            channel_data[menu_fan_ch].threshold = 100;
        }
        break;
    }
    
    // update display if necessary
    if (update_display)
    {
        update_display = false;
        OLED_clear();
        OLED_print_P(PSTR("----- Settings -----"), 0, 0, font_small);

        if (menu_level == 1)        // draw arrow for value change
        {
            OLED_print_P(PSTR(">"), 2, 0, font_small);
        }
        else if (menu_level == 2)   // draw arrow for channel change
        {
            OLED_print_P(PSTR(">"), 1, 0, font_small);
        }
        else if (menu_level == 3)   // draw arrow for tempindex change
        {
            OLED_print_P(PSTR("<"), 1, 114, font_small);
        }
        else
        {
            OLED_print_P(PSTR("<"), 0, 0, font_small);
            OLED_print_P(PSTR(">"), 0, 114, font_small);
        }
        
        switch (menu_idx)
        {
        case MI_EXIT:
            OLED_print_P(PSTR("Save & Exit ->"), 2, 0, font_small);
            break;

        case MI_VERSION:
            OLED_print_P(PSTR("TinyFanController"), 1, 0, font_small);
            OLED_print_P(PSTR("V:"), 2, 0, font_small);
            OLED_print_P(PSTR(FIRMWAREVERSION), 2, 17, font_small);
            OLED_print_P(PSTR("https://rselec.de/"), 3, 0, font_small);
            break;

        case MI_ENO:
            OLED_print_P(fstr_ENO, 1, 16, font_small);
            if (alert_data.overtempEnable != 0)
            {
                OLED_print_P(fstr_enabled, 2, 16, font_small);
            }
            else
            {
                OLED_print_P(fstr_disabled, 2, 16, font_small);
            }
            break;

        case MI_ENF:
            OLED_print_P(fstr_ENF, 1, 16, font_small);
            if (alert_data.fanblockEnable != 0)
            {
                OLED_print_P(fstr_enabled, 2, 16, font_small);
            }
            else
            {
                OLED_print_P(fstr_disabled, 2, 16, font_small);
            }
            break;

        case MI_MINT:
            OLED_printf_P(1, 16, font_small, PSTR("CH%u"), menu_fan_ch);
            OLED_print_P(fstr_MINT, 1, 34, font_small);
            OLED_printf_P(1, 82, font_small, PSTR("%u"), menu_t_idx);
            OLED_printf_P(2, 16, font_small, PSTR("%s%cC"), temp2str(channel_data[menu_fan_ch].minTemp[menu_t_idx], myString), 0xB0);
            break;

        case MI_MAXT:
            OLED_printf_P(1, 16, font_small, PSTR("CH%u"), menu_fan_ch);
            OLED_print_P(fstr_MAXT, 1, 34, font_small);
            OLED_printf_P(1, 82, font_small, PSTR("%u"), menu_t_idx);
            OLED_printf_P(2, 16, font_small, PSTR("%s%cC"), temp2str(channel_data[menu_fan_ch].maxTemp[menu_t_idx], myString), 0xB0);
            break;
            
        case MI_MAN:
            OLED_printf_P(1, 16, font_small, PSTR("CH%u"), menu_fan_ch);
            OLED_print_P(fstr_MAN, 1, 34, font_small);
            OLED_printf_P(2, 16, font_small, PSTR("%3u%%"), channel_data[menu_fan_ch].manualPower);
            break;
            
        case MI_MINP:
            OLED_printf_P(1, 16, font_small, PSTR("CH%u"), menu_fan_ch);
            OLED_print_P(fstr_MINP, 1, 34, font_small);
            OLED_printf_P(2, 16, font_small, PSTR("%3u%%"), channel_data[menu_fan_ch].minimumPower);
            break;
            
        case MI_START:
            OLED_printf_P(1, 16, font_small, PSTR("CH%u"), menu_fan_ch);
            OLED_print_P(fstr_START, 1, 34, font_small);
            OLED_printf_P(2, 16, font_small, PSTR("%3us"), channel_data[menu_fan_ch].startupTime);
            break;
            
        case MI_AUTO:
            OLED_printf_P(1, 16, font_small, PSTR("CH%u"), menu_fan_ch);
            OLED_print_P(fstr_AUTO, 1, 34, font_small);            
            if (channel_data[menu_fan_ch].automaticMode != 0)
            {
                OLED_print_P(fstr_enabled, 2, 16, font_small);
            }
            else
            {
                OLED_print_P(fstr_disabled, 2, 16, font_small);
            }
            break;
            
        case MI_STOP:
            OLED_printf_P(1, 16, font_small, PSTR("CH%u"), menu_fan_ch);
            OLED_print_P(fstr_STOP, 1, 34, font_small);            
            if (channel_data[menu_fan_ch].stopEnable != 0)
            {
                OLED_print_P(fstr_enabled, 2, 16, font_small);
            }
            else
            {
                OLED_print_P(fstr_disabled, 2, 16, font_small);
            }
            break;
            
        case MI_THR:
            OLED_printf_P(1, 16, font_small, PSTR("CH%u"), menu_fan_ch);
            OLED_print_P(fstr_THR, 1, 34, font_small);
            OLED_printf_P(2, 16, font_small, PSTR("%3u%%"), channel_data[menu_fan_ch].threshold);
            break;            
        
        default:
            menu_idx = MI_EXIT;
            update_display = true;
            break;
        }
    }
    return true;
}


void display_statusscreen(void)
{
    char myString[6] = "";	
    
    for (uint8_t i = 0; i < FAN_CHANNELS; i++)
    {
        OLED_printf_P(i, 0, font_small, PSTR("F%1u:%5urpm %3u%%"), i, channel_status[i].rpm, channel_status[i].power);
        PGM_P str_man_auto = fstr_manual;
        if (channel_data[i].automaticMode)
        {
            str_man_auto = fstr_auto;
        }		
        OLED_print_P(str_man_auto, i, 102, font_small);
    }
    
    OLED_printf_P(2, 0, font_small, PSTR("T0:%s%cC"), temp2str(temperature[0], myString), 0xB0);
    //OLED_printf_P(3, 0, font_small, PSTR("TEMP1: %s"), temp2str(tempSensor_getTemp(1), myString));
    OLED_printf_P(3, 0, font_small, PSTR("T1:%s%cC"), temp2str(temperature[1], myString), 0xB0);
    OLED_printf_P(2, 66, font_small, PSTR("T2:%s%cC"), temp2str(temperature[2], myString), 0xB0);
    

}



bool gpio_get(gpio_button_t item)
{
    switch (item)
    {
    case BUTTON_UP:				// KEY1, PB4
        return ( (PINB & (1 << 4)) != 0 );
        
    case BUTTON_DOWN:			// KEY0, PB5
        return ( (PINB & (1 << 5)) != 0 );
            
    default:
        return false;
    }	
}


void gpio_poll(void)
{
    if ( !gpio_get(BUTTON_UP) )			// switch 1 pressed
    {
        if ( buttonCtr[BUTTON_UP] >= 0x80 )	// MSB is set = first execution after button is pressed
        {
            buttonCtr[BUTTON_UP] = 0;		// Start counting from the bottom
        }
        
        if ( buttonCtr[BUTTON_UP] < 127 )	// Button count is lower maximum
        {
            buttonCtr[BUTTON_UP] ++;			// Count up
        }
    }
    else									// switch is not pressed
    {
        buttonCtr[BUTTON_UP] |= 0x80;		// set MSB, count value remains in bit 0 to 6	
    }
    
    if ( !gpio_get(BUTTON_DOWN) )			// switch 2 pressed
    {
        if ( buttonCtr[BUTTON_DOWN] >= 0x80 )	
        {
            buttonCtr[BUTTON_DOWN] = 0;		
        }
        
        if ( buttonCtr[BUTTON_DOWN] < 127 )	
        {
            buttonCtr[BUTTON_DOWN] ++;			
        }
    }
    else									
    {
        buttonCtr[BUTTON_DOWN] |= 0x80;		
    }
}


bool gpio_checkButton(gpio_button_t button, gpio_presstime_t duration)
{
    if (duration == SHORT)					// check if short press happened
    {
        // button was pressed in the past and not to long and not to short
        if (    ((buttonCtr[button] & 0x80) != 0)
             && ((buttonCtr[button] & 0x7F) < LONG_DURATION )
             && ((buttonCtr[button] & 0x7F) > SHORT_DURATION ) )
        {
            buttonCtr[button] = 0;			// reset button count
            return true;
        } 
        else
        {
            return false;
        }		
    } 
    else if (duration == LONG)
    {
        // button is pressed and longer then long duration
        if (    ((buttonCtr[button] & 0x80) == 0)
             && ((buttonCtr[button] & 0x7F) >= LONG_DURATION )
             && ((buttonCtr[button] & 0x7F) < MAX_DURATION ) )
        {
            buttonCtr[button] = MAX_DURATION;			// reset button count
            return true;
        } 
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}


// convert temperature value to string representitive
// aString must be 6 bytes long!
char* temp2str(uint8_t temp, char* aString)
{
  aString[0] = ' ';
  aString[1] = ' ';
    
  uint8_t n = temp / 2;

  if (n >= 100)
  {
    itoa(n, aString, 10);
  }
  else if (n >= 10)
  {
    itoa(n, aString+1, 10);
  }
  else
  {
    itoa(n, aString+2, 10);
  }
  
  aString[3] = DECIMAL_SEPERATOR; 

  if ((temp % 2) == 1)
  {
    aString[4] = '5';
  }
  else
  {
    aString[4] = '0';
  }
  aString[5] = 0;
  
  return aString;
}


// looks for the first value (0-127) in a string, -1 if not found
int8_t findNumberInString(char* aString)
{
    uint8_t i = 0;
    bool numberFound = false;
    int result = -1;
    while (aString[i] != '\0')	// for any char in the string
    {
        if (aString[i] >= '0' && aString[i] <= '9')	// the actual char is a digit
        {
            if (numberFound == false)	// this is the first digit so far
            {
                result = aString[i] - '0';
                numberFound = true;
            }
            else
            {
                result = result*10 + aString[i] - '0';
                if (result > 127)	// we have an overflow
                {
                    return -1;
                }				
            }
        }
        else if (numberFound)	// the actual char is not a digit but we already had a start of a number
        {
            break;				// exit while loop, don't search for more digits
        }

        i++;					// next char of string
    }
    
    return ( (int8_t)result );
}


// *IDN?
void Identify(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
  interface.print("rselec,TinyFanController,");
  interface.print(FIRMWAREVERSION);
  interface.print(SCPI_TERMINATOR);  
}



// RPM#?
void GetRpm(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    //Get the numeric suffix/index (if any) from the commands
    int8_t suffix = findNumberInString(commands.Last());

    //If the suffix is valid, print the pin's logic state to the interface
    if ( (suffix >= 0) && (suffix < FAN_CHANNELS) )
    {
          interface.print(channel_status[suffix].rpm);
          interface.print(SCPI_TERMINATOR);
    }
}   



// POWer#?
void GetPower(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    //Get the numeric suffix/index (if any) from the commands
    int8_t suffix = findNumberInString(commands.Last());

    //If the suffix is valid, print the pin's logic state to the interface
    if ( (suffix >= 0) && (suffix < FAN_CHANNELS) )
    {
          interface.print(channel_status[suffix].power);
          interface.print(SCPI_TERMINATOR);
    }
}



// Temp#?
void GetTemp(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    //Get the numeric suffix/index (if any) from the commands
    int8_t suffix = findNumberInString(commands.Last());
    //char aString[6];
    //If the suffix is valid, print the pin's logic state to the interface
    if ( (suffix >= 0) && (suffix < SENSOR_CHANNELS) )
    {
          //interface.print(temp2str(temperature[suffix], aString));
        interface.print((float)temperature[suffix]/2.0);
          interface.print(SCPI_TERMINATOR);
    }
}



// STAtus#?
void GetStatus(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    //Get the numeric suffix/index (if any) from the commands
    int8_t suffix = findNumberInString(commands.Last());

    //If the suffix is valid, print the pin's logic state to the interface
    if ( (suffix >= 0) && (suffix < FAN_CHANNELS) )
    {
          switch (channel_status[suffix].status)
        {
        case off:
            interface.print("OFF");
            break;

        case startup:
            interface.print("START");
            break;

        case on:
            interface.print("ON");
            break;
        }
          interface.print(SCPI_TERMINATOR);
    }
}



// ENOvertemp <0/DISABLE, 1/ENABLE>
void SetOvertempEnable(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    String first_parameter = String(parameters.First());
    first_parameter.toUpperCase();
    if ( (first_parameter == "ENABLE")
      || (first_parameter == "1") )
    {
        alert_data.overtempEnable = 1;
    }
    else if ( (first_parameter == "DISABLE")
           || (first_parameter == "0") )
    {
        alert_data.overtempEnable = 0;
    }
    eeprom_update_byte(&eeprom_alert_data.overtempEnable, alert_data.overtempEnable);
}



// ENOvertemp?
void GetOvertempEnable(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    interface.print(alert_data.overtempEnable);
    interface.print(SCPI_TERMINATOR);	
}



// ENFanblock <0/DISABLE, 1/ENABLE>
void SetFanblockEnable(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    String first_parameter = String(parameters.First());
    first_parameter.toUpperCase();
    if ( (first_parameter == "ENABLE")
      || (first_parameter == "1") )
    {
        alert_data.fanblockEnable = 1;
    }
    else if ( (first_parameter == "DISABLE")
           || (first_parameter == "0") )
    {
        alert_data.fanblockEnable = 0;
    }
    eeprom_update_byte(&eeprom_alert_data.fanblockEnable, alert_data.fanblockEnable);
}



// ENFanblock?
void GetFanblockEnable(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    interface.print(alert_data.fanblockEnable);
    interface.print(SCPI_TERMINATOR);
}



// OVERtemp?
void GetOvertemp(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    interface.print(alert_status.overtemp);
    interface.print(SCPI_TERMINATOR);
}



// FANBlock?
void GetFanblock(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    interface.print(alert_status.fanblock);
    interface.print(SCPI_TERMINATOR);
}



// CHannel#:MINTemp# <Celsius>
void SetMinTemp(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    //Get the numeric suffix/index (if any) from the commands
    int8_t channel = findNumberInString(commands.First());
    int8_t sensor = findNumberInString(commands.Last());

    //If the suffix is valid, print the pin's logic state to the interface
    if ( (channel >= 0) && (channel < FAN_CHANNELS) )
    {
          if ( (sensor >= 0) && (sensor < SENSOR_CHANNELS) )
        {
            // scan for vailid temperature value
            if (parameters.Size() > 0)
            {
                float temp = String(parameters[0]).toFloat();
                temp = temp * 2.0; 					// temperatures are stored as integers with 0.5 °C resolution
                channel_data[channel].minTemp[sensor] = (uint8_t)round(constrain(temp, 0.0, 250.0));
                eeprom_update_byte(&eeprom_channel_data[channel].minTemp[sensor], channel_data[channel].minTemp[sensor]);
              }
        }		  
    }
}



// CHannel#:MINTemp#?
void GetMinTemp(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    //Get the numeric suffix/index (if any) from the commands
    int8_t channel = findNumberInString(commands.First());
    int8_t sensor = findNumberInString(commands.Last());

    //If the suffix is valid, print the pin's logic state to the interface
    if ( (channel >= 0) && (channel < FAN_CHANNELS) )
    {
          if ( (sensor >= 0) && (sensor < SENSOR_CHANNELS) )
        {
            interface.print((float)channel_data[channel].minTemp[sensor]/2.0);
            interface.print(SCPI_TERMINATOR);
        }		  
    }
}



// CHannel#:MAXTemp# <Celsius>
void SetMaxTemp(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    //Get the numeric suffix/index (if any) from the commands
    int8_t channel = findNumberInString(commands.First());
    int8_t sensor = findNumberInString(commands.Last());

    //If the suffix is valid, print the pin's logic state to the interface
    if ( (channel >= 0) && (channel < FAN_CHANNELS) )
    {
          if ( (sensor >= 0) && (sensor < SENSOR_CHANNELS) )
        {
            // scan for vailid temperature value
            if (parameters.Size() > 0)
            {
                float temp = String(parameters[0]).toFloat();
                temp = temp * 2.0; 					// temperatures are stored as integers with 0.5 °C resolution
                channel_data[channel].maxTemp[sensor] = (uint8_t)round(constrain(temp, 0.0, 255.0));
                eeprom_update_byte(&eeprom_channel_data[channel].maxTemp[sensor], channel_data[channel].maxTemp[sensor]);
              }
        }		  
    }
}



// CHannel#:MAXTemp#?
void GetMaxTemp(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    //Get the numeric suffix/index (if any) from the commands
    int8_t channel = findNumberInString(commands.First());
    int8_t sensor = findNumberInString(commands.Last());

    //If the suffix is valid, print the pin's logic state to the interface
    if ( (channel >= 0) && (channel < FAN_CHANNELS) )
    {
          if ( (sensor >= 0) && (sensor < SENSOR_CHANNELS) )
        {
            interface.print((float)channel_data[channel].maxTemp[sensor]/2.0);
            interface.print(SCPI_TERMINATOR);
        }		  
    }
}



// CHannel#:MANualpower <Percent>
void SetManualPower(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    //Get the numeric suffix/index (if any) from the commands
    int8_t channel = findNumberInString(commands.First());

    //If the suffix is valid, print the pin's logic state to the interface
    if ( (channel >= 0) && (channel < FAN_CHANNELS) )
    {
          if (parameters.Size() > 0)
        {
            channel_data[channel].manualPower = (uint8_t)constrain(String(parameters[0]).toInt(), 0, 100);
            eeprom_update_byte(&eeprom_channel_data[channel].manualPower, channel_data[channel].manualPower);
        }	  
    }
}



// CHannel#:MANualpower?
void GetManualPower(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    //Get the numeric suffix/index (if any) from the commands
    int8_t channel = findNumberInString(commands.First());

    //If the suffix is valid, print the pin's logic state to the interface
    if ( (channel >= 0) && (channel < FAN_CHANNELS) )
    {
          interface.print(channel_data[channel].manualPower);
        interface.print(SCPI_TERMINATOR);	  
    }
}



// CHannel#:MINPower <Percent>
void SetMinimumPower(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    //Get the numeric suffix/index (if any) from the commands
    int8_t channel = findNumberInString(commands.First());

    //If the suffix is valid, print the pin's logic state to the interface
    if ( (channel >= 0) && (channel < FAN_CHANNELS) )
    {
          if (parameters.Size() > 0)
        {
            channel_data[channel].minimumPower = (uint8_t)constrain(String(parameters[0]).toInt(), 0, 100);
            eeprom_update_byte(&eeprom_channel_data[channel].minimumPower, channel_data[channel].minimumPower);
        }	  
    }
}



// CHannel#:MINPower?
void GetMinimumPower(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    //Get the numeric suffix/index (if any) from the commands
    int8_t channel = findNumberInString(commands.First());

    //If the suffix is valid, print the pin's logic state to the interface
    if ( (channel >= 0) && (channel < FAN_CHANNELS) )
    {
          interface.print(channel_data[channel].minimumPower);
        interface.print(SCPI_TERMINATOR);	  
    }
}



// CHannel#:STARTuptime <seconds>
void SetStartupTime(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    //Get the numeric suffix/index (if any) from the commands
    int8_t channel = findNumberInString(commands.First());

    //If the suffix is valid, print the pin's logic state to the interface
    if ( (channel >= 0) && (channel < FAN_CHANNELS) )
    {
          if (parameters.Size() > 0)
        {
            channel_data[channel].startupTime = (uint8_t)constrain(String(parameters[0]).toInt(), 0, 255);
            eeprom_update_byte(&eeprom_channel_data[channel].startupTime, channel_data[channel].startupTime);
        }	  
    }
}



// CHannel#:STARTuptime?
void GetStartupTime(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    //Get the numeric suffix/index (if any) from the commands
    int8_t channel = findNumberInString(commands.First());

    //If the suffix is valid, print the pin's logic state to the interface
    if ( (channel >= 0) && (channel < FAN_CHANNELS) )
    {
          interface.print(channel_data[channel].startupTime);
        interface.print(SCPI_TERMINATOR);	  
    }
}



// CHannel#:AUTOmaticmode <0/DISABLE, 1/ENABLE>
void SetAutomaticMode(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    //Get the numeric suffix/index (if any) from the commands
    int8_t channel = findNumberInString(commands.First());
    
    //If the suffix is valid, print the pin's logic state to the interface
    if ( (channel >= 0) && (channel < FAN_CHANNELS) )
    {
        String first_parameter = String(parameters.First());
        first_parameter.toUpperCase();
        if ( (first_parameter == "ENABLE")
          || (first_parameter == "1") )
        {
            channel_data[channel].automaticMode = 1;
        }
        else if ( (first_parameter == "DISABLE")
               || (first_parameter == "0") )
        {
            channel_data[channel].automaticMode = 0;
        }
        eeprom_update_byte(&eeprom_channel_data[channel].automaticMode, channel_data[channel].automaticMode);
    }	
}



// CHannel#:AUTOmaticmode?
void GetAutomaticMode(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    //Get the numeric suffix/index (if any) from the commands
    int8_t channel = findNumberInString(commands.First());

    //If the suffix is valid, print the pin's logic state to the interface
    if ( (channel >= 0) && (channel < FAN_CHANNELS) )
    {
          interface.print(channel_data[channel].automaticMode);
        interface.print(SCPI_TERMINATOR);	  
    }
}



// CHannel#:STOPenable <0/DISABLE, 1/ENABLE>
void SetStopEnable(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    //Get the numeric suffix/index (if any) from the commands
    int8_t channel = findNumberInString(commands.First());
    
    //If the suffix is valid, print the pin's logic state to the interface
    if ( (channel >= 0) && (channel < FAN_CHANNELS) )
    {
        String first_parameter = String(parameters.First());
        first_parameter.toUpperCase();
        if ( (first_parameter == "ENABLE")
          || (first_parameter == "1") )
        {
            channel_data[channel].stopEnable = 1;
        }
        else if ( (first_parameter == "DISABLE")
               || (first_parameter == "0") )
        {
            channel_data[channel].stopEnable = 0;
        }
        eeprom_update_byte(&eeprom_channel_data[channel].stopEnable, channel_data[channel].stopEnable);
    }	
}



// CHannel#:STOPenable?
void GetStopEnable(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    //Get the numeric suffix/index (if any) from the commands
    int8_t channel = findNumberInString(commands.First());

    //If the suffix is valid, print the pin's logic state to the interface
    if ( (channel >= 0) && (channel < FAN_CHANNELS) )
    {
          interface.print(channel_data[channel].stopEnable);
        interface.print(SCPI_TERMINATOR);	  
    }
}



// CHannel#:THReshold <Percent>
void SetThreshold(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    //Get the numeric suffix/index (if any) from the commands
    int8_t channel = findNumberInString(commands.First());

    //If the suffix is valid, print the pin's logic state to the interface
    if ( (channel >= 0) && (channel < FAN_CHANNELS) )
    {
          if (parameters.Size() > 0)
        {
            channel_data[channel].threshold = (uint8_t)constrain(String(parameters[0]).toInt(), 0, 100);
            eeprom_update_byte(&eeprom_channel_data[channel].threshold, channel_data[channel].threshold);
        }	  
    }
}



// CHannel#:THReshold?
void GetThreshold(SCPI_C commands, SCPI_P parameters, Stream& interface)
{
    //Get the numeric suffix/index (if any) from the commands
    int8_t channel = findNumberInString(commands.First());

    //If the suffix is valid, print the pin's logic state to the interface
    if ( (channel >= 0) && (channel < FAN_CHANNELS) )
    {
          interface.print(channel_data[channel].threshold);
        interface.print(SCPI_TERMINATOR);	  
    }
}

