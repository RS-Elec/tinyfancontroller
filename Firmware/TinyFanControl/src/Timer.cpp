/*********************************************************************/
/**
 * @file Timer.c
 * @brief Timer depended functions
 * @author Robert Steigemann
 *
 * project: ConFLiCT
 *********************************************************************/


#include "Timer.h"
#include <avr/io.h>
#include <avr/interrupt.h>

static volatile uint16_t period_CH[FAN_CHANNELS][FAN_BUFFER] = {0};
static volatile uint16_t oldCount_CH[FAN_CHANNELS] = {0};
static volatile uint8_t  timeout_CH[FAN_CHANNELS] = {0};

static void write_10_bit_reg(volatile uint8_t *reg, uint16_t value)
{
	TC4H = 0x03 & (uint8_t)(value>>8);
	*reg = (uint8_t)(0x00FF & value);
}


// This function initializes the timers TC1 & TC4 for 25.000 kHz PWM.
// TC2 to an other PWM frequency (>20 kHz), TC5 for period measuring and TC 3 for tick generation
void timer_init(void)
{
	// 10-Bit Timer4 for PWM generation.
	DDRB |= (1<<PB6);	// pins are outputs
	DDRD |= (1<<PD7);
	TCCR4C = (1<<COM4D1) | (1<<COM4D0) | (1<<PWM4D); // be careful writing this register, it contains shadow bits of TCCR4A, so write before TCCR4A or use bitwise or/and
	TCCR4A = (1<<COM4B1) | (1<<COM4B0) | (1<<PWM4B); // set up, clear down, phase and frequency correct PWM
	TCCR4D = (1<<WGM40);			// Phase and Frequency Correct PWM
	write_10_bit_reg(&OCR4C, (320 -1));
	// 16MHz / (2*320) = 25kHz
	// maximum is 319 so 0 - 319 are allowed values
	TCCR4B = (1<<CS40);				// Synchronous mode CK divider = 1
	

	// 16-Bit Timer1 for percision Tacho measurement
	TCCR1A = 0;				// clear register because arduino do bullshit even when not using analogWrite at all...
	TCCR1B = (1<<CS12);		// prescaler = 256
	TIMSK1 = (1<<TOIE1);	// enable Timer1 overflow interrupt

	// enable internal pullups on both interrupt pins
	// PORTD |= (1<<PD2) | (1<<PD3);	
	// both external interrupts respond to falling edges
	EICRA |= (1<<ISC31) | (1<<ISC21);
	EIMSK |= (1<<INT3) | (1<<INT2);
}


ISR(TIMER1_OVF_vect)			// Each 1048.576 ms
{
	for (uint8_t i = 0; i<FAN_CHANNELS; i++)
	{
		if (timeout_CH[i] < UINT8_MAX)
		{
			timeout_CH[i]++;	// means no fan signal since at least timeout_CH[n]-1 seconds
		}		
	}
}


ISR(INT2_vect)			// CH0
{
	uint16_t aValue = TCNT1;		// Get the counter Value
	static uint8_t i = 0;
	period_CH[0][i] = aValue - oldCount_CH[0];
	oldCount_CH[0] = aValue;
	timeout_CH[0] = 0;		// reste timeout to zero
	i++;	// iterate over buffer
	if (i == FAN_BUFFER)
	{
		i=0;
	}	
}

ISR(INT3_vect)			// CH1
{
	uint16_t aValue = TCNT1;		// Get the counter Value
	static uint8_t i = 0;
	period_CH[1][i] = aValue - oldCount_CH[1];
	oldCount_CH[1] = aValue;
	timeout_CH[1] = 0;
	i++;
	if (i == FAN_BUFFER)
	{
		i=0;
	}	
}

void timer_setPwm(uint8_t channel, uint8_t pwmValue)
{
	// check if pwmValue is a valid percent number
	if (pwmValue < 101)
	{						
		switch (channel)					// select the channel
		{
			case 0:
				write_10_bit_reg(&OCR4B, (319*pwmValue)/100);  // change the value
				break;
			case 1:
				write_10_bit_reg(&OCR4D, (319*pwmValue)/100);  // change the value
				break;
			default:
				break;
		}
	}		
}

// Get rotation per minute of a fan. A fan makes two impulses per rpm!
uint16_t timer_getRpm(uint8_t channel)
{
	if (channel >= FAN_CHANNELS)	// check vor valid channel
	{
		return 0;
	}
	
	if (timeout_CH[channel] <= 1)	// if we had a vailid signal in the last second
	{
		uint32_t period_buffer = 0;
		// make sure that reading period is not interrupted by external interrupt
		EIMSK &= ~( (1<<INT3) | (1<<INT2) );    // disable both external interrupts 
		for (uint8_t i = 0; i<FAN_BUFFER; i++)
		{
			period_buffer += period_CH[channel][i];	// sum up buffer
		}		 
		EIMSK |= (1<<INT3) | (1<<INT2);			//reenable both external interrupts 
		uint32_t dummy = period_buffer * 2 / FAN_BUFFER;
		return ((uint16_t)(3750000UL / dummy));		
	} 
	else							// No signal
	{
		return 0;
	}
}



// // Get rotation per minute of a fan. A fan makes two impulses per rpm!
// uint16_t timer_getRpm(uint8_t channel, uint32_t ms_since_last_check)
// {
// 	if (channel >= FAN_CHANNELS)	// check vor valid channel
// 	{
// 		return 0;
// 	}
	
// 	uint32_t cnt = tacho_cnt[channel];	// read volatile cnt, it's atomic
// 	tacho_cnt[channel] = 0;				// reset it, if a single pulse is in between, we don't care
	
// 	//             ms_since_last_check[s] * 2    <- 2 because of two impules per round
// 	// Tfan [s]  = --------------------------
// 	//                        cnt
// 	//
// 	//                        cnt
// 	// Ffan [Hz] = --------------------------
// 	//             ms_since_last_check[s] * 2
// 	//
// 	//                       cnt       *     60
// 	// Ffan [RPM] = ---------------------------
// 	//              ms_since_last_check[s] *  2
// 	//
// 	//                  cnt * 30 000
// 	// Ffan [RPM] = -----------------------
// 	//              ms_since_last_check[ms]

// 	uint32_t rpm = (cnt * 30000) / ms_since_last_check;
	
// 	if (rpm > UINT16_MAX)	// avoid overflow, fan with more than 32000 RPM are unrealistic
// 	{
// 		rpm = UINT16_MAX;
// 	}
// 	return ((uint16_t)rpm);
// }
