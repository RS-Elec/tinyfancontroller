/**
 * @file	OLED.h
 * @author	Robert Steigemann
 * @date	15 January 2017
 * @brief	SSD1309/SSD1306 OLED Driver library
 *
 * This library provides high level functions to use an OLED Display with OLED controller.
 * You have to configure the interface and pins in this header file.
 *
 * @license creative commons license CC BY-NC 4.0 http://creativecommons.org/licenses/by-nc/4.0/
 */

#ifndef OLED_H_
#define OLED_H_

/*#############################################################################
########################## select your interface here #########################
#############################################################################*/

//#define OLED_68XX		// 68XX 8-Bit interface, not implemented yet
//#define OLED_80XX		// 80XX 8-Bit interface, not implemented yet
//#define	OLED_SPI	// SPI (5-Wire) interface
#define OLED_I2C		// I2C interface


/*#############################################################################
######################### select your controller here #########################
#############################################################################*/

//#define SSD1309		// OLEDs with SSD1309 controller
#define SSD1306			// OLEDs with SSD1306 controller


/*#############################################################################
######################### config 68XX here, if chosen #########################
#############################################################################*/

#ifdef OLED_68XX

// TODO

#endif


/*#############################################################################
######################## config 80XX here, if chosen ##########################
#############################################################################*/

#ifdef OLED_80XX

// TODO

#endif


/*#############################################################################
######################### config SPI here, if chosen ##########################
#############################################################################*/

#ifdef OLED_SPI


#define OLED_PORT	PORTB		// The used PORT
#define OLED_DIR	DDRB		// The corresponding DDR Register

//#define OLED_SPI_I	SPID		// the used SPI port

#define OLED_CS		PORTB2			// Chip select (GPIO)
#define OLED_DC		PORTB0			// Data/Command (GPIO)
#define OLED_RES	PORTB1			// Reset (GPIO)
#define OLED_SCK	PORTB5			// Serial Clock		
#define OLED_MOSI	PORTB3			// Master Out Display In


#endif


/*#############################################################################
######################## config I2C here, if chosen ##########################
#############################################################################*/

#ifdef OLED_I2C

#define SCL_CLOCK   100000		// set I2C frequency in Hz
#define I2C_ADR     0x78		// standard 8-Bit address for SSD1306 OLED
#define OLED_PORT	PORTC		// The used PORT
#define OLED_DIR	DDRC		// The corresponding DDR Register

#define OLED_SDA	4
#define OLED_SCL	5
#define OLED_RES	-1			// Reset pin or power (GPIO), -1 if not used

#define I2C_READ    1
#define I2C_WRITE   0

#endif


/*#############################################################################
############################# select display size #############################
#############################################################################*/

#define OLED_COL		128			
#define OLED_ROW		32


/*#############################################################################
########################## enum for available fonts ###########################
#############################################################################*/

//#define EN_FONT_LARGE
//#define EN_DIGIT_BIG

typedef enum font
{
	font_small						  // small 5x7 font
#ifdef EN_FONT_LARGE
	, font_large						// large 9x16 font
#endif
#ifdef EN_DIGIT_BIG
	, digit_big							// large and wide 13x16 digits, no letters
#endif
} font_t;


/*#############################################################################
############################# function prototypes #############################
#############################################################################*/

/**
  * @brief  initialize the display
  *			This function will configure the SPI-Interface and initialize and clean the display.
  *			Have to be executed before any other display function.
  *
  * @param	rotate		rotates the complete display by 180 degrees
  * @param  contrast    specifies the contrast level at init (0...255)
  */
void OLED_init(bool rotate, uint8_t contrast);

/**
  * @brief  clears the whole display by writing 0x00 to memory
  */
void OLED_clear(void);

/**
  * @brief  set brightness of the display
  *			This function will set the pixel output current of the OLED-Display. 
  *
  * @param  contrast    specifies the contrast level (0...255)
  */
void OLED_contrast(uint8_t contrast);

/**
  * @brief  prints a string at specified position
  *
  * @param	*aString	pointer to the string to print (0 terminated)
  * @param  page		the page (line) where the string is displayed
  * @param  startColumn	the column where the string starts
  * @param  aFont       select font for printout from enum
  */
void OLED_print(char *aString, uint8_t page, uint8_t startColumn, font_t aFont);

/**
  * @brief  prints a string at specified position
  *
  * @param	aString	pointer to the PROGMEM string to print (0x00 terminated)
  * @param  page		the page (line) where the string is displayed
  * @param  startColumn	the column where the string starts
  * @param  aFont       select font for printout from enum
  */
void OLED_print_P(PGM_P aString, uint8_t page, uint8_t startColumn, font_t aFont);

/**
  * @brief  prints a formatted string at specified position
  *
  * @param  page		the page (line) where the string is displayed
  * @param  startColumn	the column where the string starts
  * @param  aFont       select font for printout from enum
  */
void OLED_printf(uint8_t page, uint8_t startColumn, font_t aFont, const char* __fmt, ...);

/**
  * @brief  prints a formatted string at specified position, format string in program memory
  *
  * @param  page		the page (line) where the string is displayed
  * @param  startColumn	the column where the string starts  
  * @param  aFont       select font for printout from enum
  */
void OLED_printf_P(uint8_t page, uint8_t startColumn, font_t aFont, PGM_P __fmt, ...);

/**
  * @brief  prints a string at specified position
  *
  * @param	*aString	pointer to the string to print (0 terminated)
  * @param  page		the page (line) where the string is displayed
  * @param  startColumn	the column where the string starts
  * @param  cursorPos   the position where the cursor is displayed (0 based from left)
  * @param  aFont       select font for printout from enum
  */
void OLED_printc(char *aString, uint8_t page, uint8_t startColumn, uint8_t cursorPos, font_t aFont);

/**
  * @brief  prints a formatted string at specified position with a cursor displayed on specific char
  *
  * @param  page		the page (line) where the string is displayed
  * @param  startColumn	the column where the string starts
  * @param  cursorPos   the position where the cursor is displayed (0 based from left)
  * @param  aFont       select font for printout from enum
  */
void OLED_printfc(uint8_t page, uint8_t startColumn, uint8_t cursorPos, font_t aFont, const char* __fmt, ...);

/**
  * @brief  prints a formatted string at specified position, format string in program memory with a cursor displayed on specific char
  *
  * @param  page		the page (line) where the string is displayed
  * @param  startColumn	the column where the string starts
  * @param  cursorPos   the position where the cursor is displayed (0 based from left)  
  * @param  aFont       select font for printout from enum
  */
void OLED_printfc_P(uint8_t page, uint8_t startColumn, uint8_t cursorPos, font_t aFont, PGM_P __fmt, ...);

/**
  * @brief  shows a picture at specified position
  *         Use the LCD Assistant tool from http://en.radzio.dxp.pl/bitmap_converter/ to generate
  *         pattern from an image. Select vertical byte orientation, little endianness and 8 pixels/byte.
  *
  * @param	*pic		pointer to the picture data generated by the Image2GLCD tool, type have to be a "const uint8_t PROGMEM" array
  * @param  startPage	the page where the picture starts, each page is 8 pixels high
  * @param  endPage		the page where the picture ends, height of the picture have to be a multiple of 8 and >= startPage
  * @param  startCol	x offset for display position 
  * @param  totalCol	equals with of the picture
  */
void OLED_picture(const uint8_t *pic, uint8_t startPage, uint8_t endPage, uint8_t startCol, uint8_t totalCol);

/**
  * @brief  draws a bar graph at specified position
  *
  * @param	percent		the filling of the graph (0-100%)
  * @param  startPage	the page where the bar graph starts, each page is 8 pixels high
  * @param  endPage		the page where the bar graph ends, height of the bar graph have to be a multiple of 8 and >= startPage
  * @param  startCol	x offset for display position 
  * @param  totalCol	equals with of the bar graph, have to be >=5
  */
void OLED_bargraph(uint8_t percent, uint8_t startPage, uint8_t endPage, uint8_t startCol, uint8_t totalCol);

/**
  * @brief  draws a diagram at specified position
  *
  * @param	values		pixel data from left to right, 0 is pixel in lowest line, maximum should be used pages*8-1
  * @param  startPage	the page where the diagram starts, each page is 8 pixels high
  * @param  endPage		the page where the diagram ends, height of the diagram have to be a multiple of 8 and >= startPage
  * @param  startCol	x offset for display position 
  * @param  totalCol	equals with of the diagram
  */
void OLED_diagram(int8_t* values, uint8_t startPage, uint8_t endPage, uint8_t startCol, uint8_t totalCol);

#endif /* OLED_H_ */