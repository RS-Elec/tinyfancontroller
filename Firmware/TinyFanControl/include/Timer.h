/*********************************************************************/
/**
 * @file Timer.h
 * @brief Timer depended functions
 * @author Robert Steigemann
 *
 * project: ConFLiCT
 *
 * @date 06.10.2013 - first implementation
 * @date 23.02.2014 - tests pass, working fine
 *********************************************************************/

#include <avr/io.h>

#ifndef TIMER_H_
#define TIMER_H_

#define FAN_CHANNELS    2
#define FAN_BUFFER      5  // lpf for tacho signal smoothing

/**
 * @brief Initializes the timers TCE0 & TCD0 for 25 kHz PWM, TCC0 for pulse measure and TCC1 for 100ms timestamps
*/
void timer_init(void);

/**
 * @brief Set the power of a PWM output
 * @param channel the channel which is set (0-1)\n
 * @param pwmValue the power in percent (0-100)
 *
 * Change the output duty cycle from 0 to 100 % of the specified channel.
*/
void timer_setPwm(uint8_t channel, uint8_t pwmValue);

/**
* @brief Get rotation per minute of a fan
* @param channel the channel to get the rpm from (0-1)
* @param ms_since_last_check the time in ms passed since last check, use millis() to calculate
* @return uint16_t the rpm of the fan connected to the channel
*/
//uint16_t timer_getRpm(uint8_t channel, uint32_t ms_since_last_check);
uint16_t timer_getRpm(uint8_t channel);

#endif /* TIMER_H_ */