/*********************************************************************/
/**
 * @file TempSensor.h
 * @brief Temperature Sensors depended functions include one wire
 * @author Robert Steigemann
 *
 * project: TFC
 *
 * Special thanks to Martin Thomas (eversmith@heizung-thomas.de)
 * and Peter Dannegger (danni@specs.de) for their one wire Code.
 * This file use modified parts of this open source code downloaded at:
 * http://siwawi.bauing.uni-kl.de/avr_projects/tempsensor/?.
 *
 * @date 12.01.2014 - first implementation
 * @date 23.02.2014 - one wire included
 * @date 08.11.2020 - rewritten for TinyFanControl project
 *********************************************************************/

#include <avr/io.h>
#include "ds18x20.h"


#ifndef TEMPSENSOR_H_
#define TEMPSENSOR_H_

#define SENSOR_CHANNELS     3           // amount of available sensor channels (0-2)

#define SUPPLY_VOLTAGE_5V   4.9802      // insert measured voltage of the 5V supply here to improve analog sensor accuarcy

// ---------------- Definitions for Sensor T0 ------------------

#define SENSOR_T0_TYPE_KTY
//#define SENSOR_T0_TYPE_NTC
//#define SENSOR_T0_TYPE_DS18X20
//#define SENSOR_T0_DISABLED

#ifdef SENSOR_T0_TYPE_KTY
    #define SENSOR_T0_RP    4679.6      // use measured value for higher accuarcy
    #define SENSOR_T0_R25   2000.0
    #define SENSOR_T0_A     7.874e-3  
    #define SENSOR_T0_B     1.874e-5
#endif

#ifdef SENSOR_T0_TYPE_NTC
    #define SENSOR_T0_RP    27000.0     // use measured value for higher accuarcy
    #define SENSOR_T0_R25   10000.0
    #define SENSOR_T0_B     3730.0
#endif

#ifdef SENSOR_T0_TYPE_DS18X20
    #define SENSOR_T0_FAMILY_CODE   DS18B20_FAMILY_CODE     // options: DS18S20_FAMILY_CODE, DS18B20_FAMILY_CODE, DS1822_FAMILY_CODE
#endif

// ---------------- Definitions for Sensor T1 ------------------

//#define SENSOR_T1_TYPE_KTY
//#define SENSOR_T1_TYPE_NTC
//#define SENSOR_T1_TYPE_DS18X20
#define SENSOR_T1_DISABLED

#ifdef SENSOR_T1_TYPE_KTY
    #define SENSOR_T1_RP    4700.0      // use measured value for higher accuarcy
    #define SENSOR_T1_R25   2000.0
    #define SENSOR_T1_A     7.874e-3  
    #define SENSOR_T1_B     1.874e-5
#endif

#ifdef SENSOR_T1_TYPE_NTC
    #define SENSOR_T1_RP    27000.0     // use measured value for higher accuarcy
    #define SENSOR_T1_R25   10000.0
    #define SENSOR_T1_B     3730.0
#endif

#ifdef SENSOR_T1_TYPE_DS18X20
    #define SENSOR_T1_FAMILY_CODE   DS18S20_FAMILY_CODE     // options: DS18S20_FAMILY_CODE, DS18B20_FAMILY_CODE, DS1822_FAMILY_CODE
#endif


// -------------------- Sensor Examples ------------------
// KTY81-210 by NXP:
//  RP   4700.0
//  R25  2000.0
//  A    7.874e-3
//  B    1.874e-5
//
// typical 10K NTC (used on many cheap fan controls):
//  RP   27000.0
//  R25  10000.0
//  B     3730.0
//
// 10K NTC by Aquacomputer:
//  RP   27000.0
//  R25  10000.0
//  B     3940.0
//
// Make sure that the correct pullup resistor (R2, R3) is soldered for your sensor.
// A 10µF cap (C1, C2) should be in place when using analog sensors.
//
// For digital DS18x20 sensors use 4k7 pullup (R2, R3) without any cap (C1, C2)!
// DS18B20 and DS18S20 sensors are supported and tested, DS1822 may work but untested.
// Make sure to select the correct family code for the used sensor.
// My DS18S20 sensors in TO-92 package are marked DS1820 only!
// Only one digital sensor per connector supported (IDs are ignored)
// Sensors must be powered via 3rd connection, parasite power option is not supported.



/**
* @brief Initialize the temperature sensors.
*
* Do this before using an other function.\n
*/
void tempSensor_init(void);

/**
* @brief Get the temperature of a sensor.
* @param channel The channel to get the temperature from (0-2).
* @return uint8_t The temperature in (°C x 2) (0-254 = 0-127°C),\n
*                 255 if sensor is not connected. 
*/
uint8_t tempSensor_getTemp(uint8_t channel);

/**
* @brief Get the amount of connected one wire sensors.
* @return uint8_t The amount of connected one wire sensors (0-8).
*/
//uint8_t tempSensor_getOneWireAmount(void);

/**
* @brief Get a part of an ID of a one wire sensor.
* @param index The index of the part of the ID (0-63).
* @return uint8_t One byte of a one wire ID.
*
* Each connected sensor has his own ID. Each ID is 8 bytes long.\n
* The IDs are organized one after each other. So the returned bytes
* of index 0-7 contains the ID of the first sensor. The ID of the second
* sensor are returned by index 8-15 and so on. If you ask for an ID of a
* sensor, which is not connected, the returned bytes are not specified.
*/
//uint8_t tempSensor_getOneWireID(uint8_t index);


#endif /* TEMPSENSOR_H_ */